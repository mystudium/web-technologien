import {FileSelectDirective, FileDropDirective } from 'ng2-file-upload'; 
import { NgModule } from '@angular/core'; 
import { CommonModule } from '@angular/common'; 

@NgModule( {
imports:[
    CommonModule
  ],
declarations:[
  FileSelectDirective,
  FileDropDirective
],
exports:[
  FileSelectDirective,
  FileDropDirective
]
})
export class SharedModule {}
