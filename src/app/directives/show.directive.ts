import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
    selector: '[show]'
})
export class ShowDirective {

    constructor(private el: ElementRef) { }

    @Input() show: string;

    @HostListener('click') onMouseClick() {
        this.showStuff(this.show);
    }

    private showStuff(d: string) {
        this.el.nativeElement.style.display = d;
    }
}