interface AuthConfiguration {
    clientID: string,
    domain: string
}

export const AUTH_CONFIG: AuthConfiguration = {
    clientID: 'Hb9mR4QoAYzF79Oq5gRRGSNHR3iaEhxh',
    domain: 'tmhk.eu.auth0.com'
};
