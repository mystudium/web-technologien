import { Component, OnInit } from '@angular/core';
import {CustomerService} from "../../services/customer/customer.service";
import {UserService} from "../../services/user/user.service";
import {User} from "../../models/user/user.model";
import {Customer} from "../../models/customer/customer.model";
import {PasswordService} from "../../services/password/password.service";
import { FileUploader } from 'ng2-file-upload';
import {AuthService} from "../../services/auth/auth.service";
import {BoxConfig, BoxHttp} from "../../services/box/box-client.service";
import {Headers, RequestOptions} from '@angular/http';
import {BOX_CONFIG} from "../../config/box/box.config";
import {Photographer} from "../../models/photographer/photographer.model";
import {PhotographerService} from "../../services/photographer/photographer.service";


const URL = 'http://localhost:4200/api/uploadnew';
@Component({
  selector: 'box-new-customer',
  templateUrl: './new-customer.component.html',
  styleUrls: ['./new-customer.component.css'],
    providers: [CustomerService, UserService, PhotographerService, PasswordService, AuthService]
})
export class NewCustomerComponent implements OnInit {
    passwd: string;
    filesToUpload: File[] = [];
    uploader: FileUploader = new FileUploader({url: URL});
    logoId: string;
    photographers: Photographer[];
    constructor(private cosService: CustomerService, private userService: UserService, private photoService: PhotographerService,
                private passwdService: PasswordService, private authService: AuthService, private boxHttp: BoxHttp) { }

    ngOnInit() {
        this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any) => {
            var responsePath = JSON.parse(response);
            if (status === 200) {
                this.logoId = responsePath.id;
            }else {
                console.log(responsePath);
            }
        };
        this.photoService.getPhotographers()
            .subscribe(
                photographers => {this.photographers = photographers; console.log(photographers); },
                err => console.log(err)
            );
    }

    onGeneratePassword() {
        this.passwdService.getPassword()
            .subscribe(
                passwd => this.passwd = passwd,
                err => console.log(err)
            );
    }

    addFotograf(event: any, fotograf: Photographer) {
        console.log(event, fotograf);
    }

    onAddNewCustomer(firmenname: string, anrede: string, vorname: string,
                     nachname: string, strasse: string,
                     hausnr: string, plz: number, stadt: string,
                     land: string, email: string, telefonnr: string,
                     handynr: string, fax: string, fotografen: Photographer[]) {
        /*
        const tok = JSON.parse(localStorage.getItem(`${BOX_CONFIG.boxTokenStorageKey}.${this.authService.getUserId()}`))
        const token = this.authService.getAccessToken();
        console.log('xxxxxxxxxxxxxxxxxxxxxxxxx');
        console.log(tok.access_token);
        console.log('-------------------------');
        console.log(token);
        const headers = new Headers({'Content-Type': 'application/json', 'Authorization' : 'Bearer ' + tok.access_token});
        console.log(headers);
        console.log('xxxxxxxxxxxxxxxxxxxxxxxxx');
        const options = new RequestOptions({headers: headers});
        --
        this.boxHttp.post('/users', {login: email, name: firmenname})
            .subscribe(
                answer => console.log(answer),
                err => console.log(err),
                () => console.log('Post')
            );*/
        console.log(anrede);
        console.log(fotografen);
        const ids: string[] = [];
        for (const fotograf of fotografen){
            ids.push(fotograf._id);
        }
        console.log(ids);
        this.userService.saveUser(new User('Username-Password-Authentication', true, email, this.passwd, 'tmhk', 'customer'))
            .subscribe(
                () => {
                    this.cosService.saveCustomer(new Customer('', firmenname, anrede, vorname, nachname, strasse,
                        hausnr, plz, stadt, land, email, telefonnr, handynr, fax, this.logoId, ids))
                        .subscribe(
                            id => {
                                const extracted = id.json();
                                console.log('New Customer ID: ');
                                console.log(extracted.resp._id);
                                for (const fotograf of fotografen){
                                    this.photoService.updateCustomerList(fotograf, extracted.resp._id)
                                        .subscribe(
                                            () => console.log('Success'),
                                            err => console.log(err)
                                        );
                                }
                            },
                            err => console.log(err),
                            () => console.log('Success')
                        );
                },
                err => console.log(err)
            );
    }
}
