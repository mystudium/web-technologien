import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhotographerlistViewComponent } from './photographerlist-view.component';

describe('PhotographerlistViewComponent', () => {
  let component: PhotographerlistViewComponent;
  let fixture: ComponentFixture<PhotographerlistViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhotographerlistViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhotographerlistViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
