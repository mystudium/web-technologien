import { Component, OnInit } from '@angular/core';
import {PhotographerService} from '../../services/photographer/photographer.service';
import {Photographer} from '../../models/photographer/photographer.model';

@Component({
  selector: 'box-photographerlist-view',
  templateUrl: './photographerlist-view.component.html',
  styleUrls: ['./photographerlist-view.component.css'],
    providers: [PhotographerService]
})
export class PhotographerlistViewComponent implements OnInit {
    photographers: Photographer[] = [];

  constructor(private photoService: PhotographerService) { }

  ngOnInit() {
      this.photoService.getPhotographers()
          .subscribe(
              photographers => this.photographers = photographers,
              error => console.log(error)
          );
  }

}
