import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {CustomerService} from "../../services/customer/customer.service";
import {Customer} from "../../models/customer/customer.model";
import {findReadVarNames} from "@angular/compiler/src/output/output_ast";
import {PhotographerService} from "../../services/photographer/photographer.service";
import {Photographer} from "../../models/photographer/photographer.model";

@Component({
  selector: 'box-customer-edit',
  templateUrl: './customer-edit.component.html',
  styleUrls: ['./customer-edit.component.css'],
    providers: [CustomerService, PhotographerService]
})
export class CustomerEditComponent implements OnInit {
    customer: Customer;
    isLoading: boolean;
    photographers: Photographer[] = [];
  constructor(private route: ActivatedRoute, private cosService: CustomerService, private photoService: PhotographerService) { }

  ngOnInit() {
      this.isLoading = true;
      this.route.params
          .switchMap((params: Params) => {
              const id = params['id'] || '0';
              console.log(id);
              return this.cosService.getCustomer(id);
          })
          .subscribe(
              cos => {
                  console.log('onInit' + cos);
                  this.isLoading = false;
                  this.customer = cos;
                  },
              err => console.log(err)
          );
      this.photoService.getPhotographers()
          .subscribe(
              photographers => {this.photographers = photographers; console.log(photographers)},
              err => console.log(err)
          );
  }

  onEditCustomer(firmenname: string, anrede: string, vorname: string, nachname: string, strasse: string, hausnr: string,
                 plz: number, stadt: string, land: string, telefonnr: string, handynr: string,
                 fax: string, fotografen: Photographer[]) {
      console.log(fotografen);
      const ids: string[] = [];
      for (const fotograf of fotografen) {
          ids.push(fotograf._id);
      }
      const tmp = new Customer(this.customer._id, firmenname, anrede, vorname, nachname, strasse, hausnr, plz, stadt, land,
      this.customer.email, telefonnr, handynr, fax, this.customer.logo, ids);
      if (this.hasChanged(firmenname, anrede, vorname, nachname, strasse, hausnr, plz, stadt, land, telefonnr, handynr, fax, ids)) {
          console.log('update Customer');
          this.cosService.updateCustomer(tmp)
              .subscribe(
                  () => console.log('Success'),
                  err => console.log(err)
              );
      }
  }

    hasChanged(firmenname: string, anrede: string, vorname: string, nachname: string, strasse: string, hausnr: string,
                   plz: number, stadt: string, land: string, telefonnr: string, handynr: string,
                   fax: string, ids: string[]) {
        const tmp = new Customer(this.customer._id, firmenname, anrede, vorname, nachname, strasse, hausnr, plz, stadt, land,
            this.customer.email, telefonnr, handynr, fax, this.customer.logo, ids);
        console.log(tmp);
        console.log(this.customer);
        console.log(this.customer.equal(tmp));
        return !this.customer.equal(tmp);
    }
}
