import {Component, Input, OnInit} from '@angular/core';
import {BillService} from '../../services/bill/bill.service';
import {CustomerService} from '../../services/customer/customer.service';
import {Bill} from '../../models/bill/bill.model';
import {Customer} from '../../models/customer/customer.model';
import {Router} from '@angular/router';
import { AuthService } from '../../services/auth/auth.service';


@Component ({
    selector: 'box-bill-view',
    templateUrl: './bill-view.component.html',
    styleUrls: ['./bill-view.component.css'],
    providers: [BillService, CustomerService]
})

export class BillViewComponent implements OnInit {
    bills: Bill[] = [];
    customers: Customer[] = [];
    filesToUpload: Array<File> = [];
    filepath: string = '';
    isLoading: boolean;

    constructor(private billService: BillService, private customerService: CustomerService,
                private router: Router, private auth: AuthService) {}

    ngOnInit() {
        this.isLoading = true;
        this.billService.getBills()
            .subscribe(
                bills => this.bills = bills,
                error => console.log(error)
            );
        this.customerService.getCustomers()
            .subscribe(
                customers => this.customers = customers,
                error => console.log(error)
            );
    }

    onChange(fileInput: any) {
        this.filesToUpload = <Array<File>> fileInput.target.files;
    }

    onAddBill(f_name, f_date, f_sum){
        const firstname = f_name.split(' ')[0];
        const lastname = f_name.split(' ')[1];
        console.log(firstname + '    ' + lastname);
        this.filepath =  firstname + "." + lastname + "/" + this.filesToUpload[0].name;
        const temp_date = f_date.split('.');
        const date = new Date(temp_date[2], temp_date[1], temp_date[0]);
        const paid = false;
        const bill = new Bill(this.filepath, date, paid, f_sum);

        this.billService.getBillsByCondition(this.filepath)
            .subscribe(
                (billArray) => {
                    let existingBills: Bill[];
                    existingBills = billArray;
                    if(existingBills.length > 0){
                        document.getElementById('popup').style.display = 'block';
                        document.getElementById('bill_form').setAttribute('class', 'not-active');
                    } else {
                        this.bills.push(bill);
                        this.billService.setBill(bill)
                            .subscribe(
                                (path:any) => {
                                    this.billService.uploadBill([{'path': path}], this.filesToUpload).toPromise().then((result) => {
                                        console.log(result);
                                    }, (error) => {
                                        console.error(error);
                                    })
                                },
                                error => console.log(error)
                            )
                    }
                }
            );
    }

    onUpdateBill(f_sum) {
        this.billService.updateBills({condition: {'filepath': this.filepath}, update: {'sum': f_sum}})
            .subscribe(
                (path:any) => {
                    this.billService.uploadBill([{'path': path}], this.filesToUpload).toPromise().then((result) => {
                        console.log(result);
                    }, (error) => {
                        console.error(error);
                    })
                }
            );
        document.getElementById('popup').style.display = 'none';
        document.getElementById('bill_form').setAttribute('class', 'active');
    }

    onPopupClose() {
        document.getElementById('popup').style.display = 'none';
        document.getElementById('bill_form').setAttribute('class', 'active');
    }

    cancelBill() {
        this.router.navigate(['/home']);
    }
}
