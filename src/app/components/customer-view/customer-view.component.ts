import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../../services/customer/customer.service';
import {Customer} from '../../models/customer/customer.model';
import {ActivatedRoute, Params} from "@angular/router";
import {UserService} from "../../services/user/user.service";
import {User} from "../../models/user/user.model";

@Component({
  selector: 'box-costumer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.css'],
  providers: [CustomerService, UserService]
})
export class CustomerViewComponent implements OnInit {
  customers: Customer[] = [];
  isLoading: boolean;
  user: User;

  constructor(private cosService: CustomerService, private userService: UserService) { }

  ngOnInit() {
    this.isLoading = true;
    this.cosService.getCustomers()
        .subscribe(
            customers => this.customers = customers,
            error => console.log(error)
        );
  }

  private delFromList(del: Customer) {
      const index: number = this.customers.indexOf(del);
      if (index !== -1) {
          this.customers.splice(index, 1);
      }
  }

  onDeleteCustomer(cos: Customer) {
    console.log(cos._id);
    this.userService.findByEmail(cos.email)
        .subscribe(
            usr => {
              this.user = usr;
              this.userService.deleteUser(cos.email)
                  .subscribe(
                      res => {
                        console.log(res);
                        this.cosService.deleteCustomer(cos._id)
                            .subscribe(
                                resp => {
                                    this.delFromList(cos);
                                  console.log('Deleted successfully');
                                },
                                err => {
                                  console.log(err);
                                  this.userService.saveUser(this.user);
                                }
                            );
                      },
                      err => console.log(err)
                  );
              },
            err => console.log('User does not exist')
        );
  }
}
