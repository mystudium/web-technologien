import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user/user.service';
import {User} from '../../models/user/user.model';

@Component({
  selector: 'box-users-view',
  templateUrl: './userlist-view.component.html',
  styleUrls: ['./userlist-view.component.css'],
  providers: [UserService]
})
export class UserListViewComponent implements OnInit {
  users: User[] = [];
  constructor(private userService: UserService) { }

  ngOnInit() {
    this.userService.getUsers()
        .subscribe(
            users => this.users = users,
            error => console.log(error)
        );
  }
}
