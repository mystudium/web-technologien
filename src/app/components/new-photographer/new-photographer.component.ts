import { Component, OnInit } from '@angular/core';
import {PhotographerService} from "../../services/photographer/photographer.service";
import {UserService} from "../../services/user/user.service";
import {PasswordService} from "../../services/password/password.service";
import {User} from "../../models/user/user.model";
import {Photographer} from "../../models/photographer/photographer.model";

@Component({
  selector: 'box-new-photographer',
  templateUrl: './new-photographer.component.html',
  styleUrls: ['./new-photographer.component.css'],
  providers: [PhotographerService, UserService, PasswordService]
})
export class NewPhotographerComponent implements OnInit {
  passwd: string;

  constructor(private photoService: PhotographerService, private userService: UserService, private passwdService: PasswordService) { }


  ngOnInit() {
  }

  onGeneratePassword() {
    this.passwdService.getPassword()
        .subscribe(
            passwd => this.passwd = passwd,
            err => console.log(err)
        );
  }

  onAddNewPhotographer(anrede: string, vorname: string, nachname: string, strasse: string, hausnr: string, plz: number,
                       stadt: string, land: string, email: string, telefonnr: string, handynr: string, fax: string) {
    this.userService.saveUser(new User('Username-Password-Authentication', true, email, this.passwd, 'tmhk', 'photographer'))
        .subscribe(
            () => {
              this.photoService.savePhotographer(new Photographer('', anrede, vorname, nachname, strasse, hausnr, plz, stadt,
                  land, email, telefonnr, handynr, fax, []))
                  .subscribe(
                      () => console.log('Success'),
                      err => console.log(err)
                  );
            },
            err => console.log(err)
        );
  }
}
