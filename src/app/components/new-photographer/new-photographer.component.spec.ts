import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewPhotographerComponent } from './new-photographer.component';

describe('NewPhotographerComponent', () => {
  let component: NewPhotographerComponent;
  let fixture: ComponentFixture<NewPhotographerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewPhotographerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewPhotographerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
