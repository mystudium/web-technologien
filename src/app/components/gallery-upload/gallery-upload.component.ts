import { Component, Input, OnInit, Provider, OnChanges} from '@angular/core';
import {CustomerService} from '../../services/customer/customer.service';
import {Customer} from '../../models/customer/customer.model';
import {FileUploader } from 'ng2-file-upload';

const URL = 'http://localhost:4200/api/testapi';

@Component({
  selector: 'box-gallery-upload',
  templateUrl: './gallery-upload.component.html',
  styleUrls: ['./gallery-upload.component.css'],
  providers: [CustomerService]
})
export class GalleryUploadComponent implements OnInit {
  customers: Customer[] = [];
  filesToUpload: File[] = [];
  uploader: FileUploader = new FileUploader({url: URL});
  hasBaseDropZoneOver: boolean = false;
  hasAnotherDropZoneOver: boolean = false;
 
  fileOverBase(e:any):void {
    this.hasBaseDropZoneOver = e;
  }
 
  fileOverAnother(e:any):void {
    this.hasAnotherDropZoneOver = e;
  };
  constructor(private cusService: CustomerService) { }

  ngOnInit() {
    this.cusService.getCustomers()
      .subscribe(
        customers => this.customers = customers,
        error => console.log(error)
      );
    this.uploader.onCompleteItem = (item: any, response: any, status: any, headers: any)=>{
      const responsePath = JSON.parse(response);
      console.log (responsePath.id)
    }
  }
}
