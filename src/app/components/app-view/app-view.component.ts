import { Component } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';

@Component({
  selector: 'box-root',
  templateUrl: './app-view.component.html',
  styleUrls: ['./app-view.component.css']
})

export class AppViewComponent {
  title = 'box works!';
  constructor(private auth: AuthService) { 
    auth.handleAuthentication();
  }
}
