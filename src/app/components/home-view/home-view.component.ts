import { Component, OnInit } from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";
import {Router} from '@angular/router';

@Component({
  selector: 'box-home-view',
  templateUrl: './home-view.component.html',
  styleUrls: ['./home-view.component.css'],
})
export class HomeViewComponent implements OnInit {
  isLoading: boolean;
  constructor(private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.isLoading = false;
  }

  toBills() {
      this.router.navigate(['/bill']);
  }

  toUsers() {
    this.router.navigate(['/users']);
  }

  toCustomers() {
        this.router.navigate(['/customer']);
  }
  toPhotographers() {
        this.router.navigate(['/photographers']);
  }
}
