import { BillViewComponent } from './components/bill-view/bill-view.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './services/auth/auth.guard';
import { WelcomeViewComponent } from './components/welcome-view/welcome-view.component';
import { UserViewComponent } from './components/user-view/user-view.component';
import { FolderViewComponent } from './components/folder-view/folder-view.component';
import { CallbackComponent } from './components/callback/callback.component';
import { BoxExplorerComponent } from "./components/box-explorer/box-explorer.component";
import { FileDetailViewComponent } from "./components/file-detail-view/file-detail-view.component";
import {CustomerViewComponent} from "./components/customer-view/customer-view.component";
import {HomeViewComponent} from "./components/home-view/home-view.component";
import {UserListViewComponent} from "./components/userlist-view/userlist-view.component";
import {NewCustomerComponent} from "./components/new-customer/new-customer.component";
import {NewPhotographerComponent} from "./components/new-photographer/new-photographer.component";
import {PhotographerlistViewComponent} from "./components/photographerlist-view/photographerlist-view.component";
import { GalleryUploadComponent} from './components/gallery-upload/gallery-upload.component';
import {NavbarComponent} from "./components/navbar/navbar.component";
import {CustomerEditComponent} from "./components/customer-edit/customer-edit.component";
const routes: Routes = [
  {
    path: '',
    children: [
        {path: '', component: WelcomeViewComponent},
        {path: '', component: NavbarComponent, outlet: 'header'}
    ],
  },
  {
    path: 'home',
    children: [
      {path: '', component: HomeViewComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'upload',
    component: GalleryUploadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'customer',
    children: [
      {path: '', component: CustomerViewComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
      canActivate: [AuthGuard]
  },
  {
    path: 'users',
    children: [
      {path: '', component: UserListViewComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'photographers',
    children: [
      {path: '', component: PhotographerlistViewComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
    canActivate: [AuthGuard]
  },
  {
      path: 'addCustomer',
      children: [
          {path: '', component: NewCustomerComponent},
          {path: '', component: NavbarComponent, outlet: 'header'}
      ],
      canActivate: [AuthGuard]
  },
  {
    path: 'editCustomer/:id',
    children: [
      {path: '', component: CustomerEditComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'addPhotographer',
    children: [
      {path: '', component: NewPhotographerComponent},
      {path: '', component: NavbarComponent, outlet: 'header'}
    ],
    canActivate: [AuthGuard]
  },
  {
    path: 'callback',
    component: CallbackComponent
  },
    {
    path: 'upload',
    component: GalleryUploadComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'explorer',
    component: BoxExplorerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'explorer/folder/',
    component: BoxExplorerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'explorer/folder/:id',
    component: BoxExplorerComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'explorer/file/:id',
    component: FileDetailViewComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'bill',
    component: BillViewComponent,
    canActivate:[AuthGuard]
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
