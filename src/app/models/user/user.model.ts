export class User {
    constructor(public connection: string, public debug: boolean, public email: string, public password: string, public tenant: string, public role: string) { }

    public toString() {
        return this.connection + this.email + this.tenant;
    }
}