import { BoxFolder } from './../box/box-folder.model';
import {Photographer} from "../photographer/photographer.model";
export class Customer {

    constructor(public _id: string, public firmenname: string, public anrede: string, public vorname: string,
                public nachname: string, public strasse: string,
                public hausnr: string, public plz: number, public stadt: string,
                public land: string, public email: string, public telefonnr: string,
                public handynr: string, public fax: string, public logo: string, public fotografen: string[]) { }


    public toString() {
        return this.firmenname + ' ' + this.email;
    }

    public equal(this, other) {
        return (this + '' === other + '') && (this.fotografen + '' === other.fotografen + '');
    }
}
