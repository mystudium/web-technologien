export class Photographer {
    constructor(public _id: string, public anrede: string, public vorname: string, public nachname: string, public strasse: string,
        public hausnr: string, public plz: number, public stadt: string, public land: string, public email: string,
        public telefonnr: string, public handynr: string, public fax: string, public kunden: string[]) { }

    public toString() {
        return this.anrede + ' ' + this.nachname;
    }
}