export class Bill{
    constructor(public filepath: string, public date: Date, public paid: boolean, public sum: Number){}

    public toString = () : string => {
        return this.filepath + ' ' + this.date + '\n' + this.paid + '\n' + this.sum;
    }
}

