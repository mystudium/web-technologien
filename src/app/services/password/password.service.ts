import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class PasswordService {

  constructor(private http: Http) { }

  getPassword(): Observable<any> {
    return this.http.get('http://localhost:4200/api/generatePassword')
        .map((data: Response) => {
          const extracted = data.json(); // data of the Response converted in js-Object
          return extracted.data; // return transformed data
        }); // Transform data from server
  }
}
