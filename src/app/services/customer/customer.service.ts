import { BoxFolder } from './../../models/box/box-folder.model';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Customer} from '../../models/customer/customer.model';
import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http';


@Injectable()
export class CustomerService {

  constructor(private http: Http) {}

  getCustomers(): Observable<any> {
    // url of resource -> routes/app.js router.get -> data: messages
    return this.http.get('http://localhost:4200/api/customers')
        .map((data: Response) => {
          const extracted = data.json(); // data of the Response converted in js-Object
            console.log(extracted);
          const cosArray: Customer[] = [];
          let cos;
          for (const element of extracted.data){
            cos = new Customer(element._id, element.firmenname, element.anrede, element.vorname, element.nachname,
                element.strasse, element.hausnr, element.plz, element.stadt, element.land, element.email, element.telefonnr,
                element.handynr, element.fax, element.logo, element.fotografen);
            cosArray.push(cos);
          }
          return cosArray; // return transformed data

        }); // Transform data from server
  }

  saveCustomer(cos: Customer): Observable<any>{
    // Creating body for post-method in routes/app.js
    const body = JSON.stringify(cos);
    const headers = new Headers({'Content-Type': 'application/json'});
    // sends the request only if its subscribed --> Observable
    // url of post-method in routes/app.js
    return this.http.post('http://localhost:4200/api/customer', body, {headers: headers});
  }

    getCustomer(id: string): Observable<any> {
      console.log('Customer with id' + id);
        const headers = new Headers({'Content-Type': 'application/json'});
        const params = new URLSearchParams();
        params.append('_id', id);
        const options = new RequestOptions({headers: headers, search: params});

        return this.http.get('http://localhost:4200/api/customerById', options)
            .map((data: Response) => {
                const extracted = data.json(); // data of the Response converted in js-Object
                const element = extracted.data[0];
                const cus = new Customer(element._id, element.firmenname, element.anrede, element.vorname, element.nachname,
                    element.strasse, element.hausnr, element.plz, element.stadt, element.land,
                    element.email, element.telefonnr, element.handynr, element.fax, element.logo, element.fotografen);
                return cus;
            });
    }
      uploadLogo(params: Array<any>, file: File): Observable<any>{
        return Observable.create(observer => {
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();
            formData.append("upload", file, file.name);
            formData.append("path", params[0].path);
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            };
            xhr.open("POST", 'http://localhost:4200/api/logoupload', true);
            xhr.send(formData);
        });
    }

    updateCustomer(cos: Customer): Observable<any>{
        // Creating body for post-method in routes/app.js
        const body = JSON.stringify(cos);
        const headers = new Headers({'Content-Type': 'application/json'});
        // sends the request only if its subscribed --> Observable
        // url of post-method in routes/app.js
        return this.http.post('http://localhost:4200/api/updateCustomer', body, {headers: headers});
    }

    deleteCustomer(id: string): Observable<any>{
        // Creating body for post-method in routes/app.js
        const body = {_id : id};
        const headers = new Headers({'Content-Type': 'application/json'});
        // sends the request only if its subscribed --> Observable
        // url of post-method in routes/app.js
        return this.http.post('http://localhost:4200/api/deleteCustomer', body, {headers: headers});
    }
}
