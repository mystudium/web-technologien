import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Http, Response, Headers, URLSearchParams, RequestOptions} from '@angular/http';
import {User} from '../../models/user/user.model';

@Injectable()
export class UserService {

  constructor(private http: Http) { }

  getUsers(): Observable<any> {
    // url of resource -> routes/app.js router.get -> data: messages
    return this.http.get('http://localhost:4200/api/users')
        .map((data: Response) => {
          const extracted = data.json(); // data of the Response converted in js-Object
          console.log(extracted);
          const userArray: User[] = [];
          let user;
          for (const element of extracted.data){
            user = new User(element.connection, element.debug, element.email, element.password, element.tenant, element.role);
            userArray.push(user);
          }
          return userArray; // return transformed data

        }); // Transform data from server
  }

    saveUser(user: User): Observable<any> {
        // Creating body for post-method in routes/app.js
        const body = JSON.stringify(user);
        const headers = new Headers({'Content-Type': 'application/json'});
        // sends the request only if its subscribed --> Observable
        // url of post-method in routes/app.js
        return this.http.post('http://localhost:4200/api/user', body, {headers: headers});
    }

    findByEmail(email: string): Observable<any> {
        const headers = new Headers({'Content-Type': 'application/json'});
        const params = new URLSearchParams();
        params.append('email', email);
        const options = new RequestOptions({headers: headers, search: params});

        return this.http.get('http://localhost:4200/api/userByEmail', options)
            .map((data: Response) => {
                const extracted = data.json(); // data of the Response converted in js-Object
                const element = extracted.data;
                const user = new User(element.connection, element.debug, element.email, element.password, element.tenant, element.role);
                return user;
            });
    }

    deleteUser(email: string): Observable<any> {
        // Creating body for post-method in routes/app.js
        const body = {email: email};
        const headers = new Headers({'Content-Type': 'application/json'});
        // sends the request only if its subscribed --> Observable
        // url of post-method in routes/app.js
        return this.http.post('http://localhost:4200/api/deleteUser', body, {headers: headers});
    }
}
