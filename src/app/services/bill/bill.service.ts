import { Injectable } from '@angular/core'
import {Http, Headers, Response} from "@angular/http";
import 'rxjs/Rx';
import {Observable} from "rxjs/Observable";
import {Bill} from "../../models/bill/bill.model";

@Injectable()
export class BillService {

    constructor(private http: Http) {}

    getBills():Observable<any>{
        return this.http.get('http://localhost:4200/api/bills')
            .map((data: Response) => {
                const extracted = data.json(); //data of the Response converted in js-Object
                const billArray: Bill[] = [];
                let bill;
                for(let element of extracted.data){
                    bill = new Bill(element.filepath, element.date, element.paid, element.sum);
                    billArray.push(bill);
                }
                return billArray; //return transformed data

            }); //Transform data from server
    }

    updateBills(data):Observable<any>{
        const body = {condition: data.condition, update: data.update};
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post('http://localhost:4200/api/updateBills', body, {headers: headers})
            .map((data: Response) => {
                const extracted = data.json(); //data of the Response converted in js-Object
                console.log(extracted.path);
                return extracted.path;
            });
    }

    getBillsByCondition(condition:string):Observable<any>{
        const body = {condition: condition};
        const headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post('http://localhost:4200/api/billsByCondition', body, {headers: headers})
            .map((data: Response) => {
                const extracted = data.json(); //data of the Response converted in js-Object
                const billArray: Bill[] = [];
                let bill;
                for(let element of extracted.data){
                    bill = new Bill(element.filepath, element.date, element.paid, element.sum);
                    billArray.push(bill);
                }
                return billArray; //return transformed data

            }); //Transform data from server
    }

    setBill(bill: Bill): Observable<any> {
        //Creating body for post-method in routes/app.js
        const body = JSON.stringify(bill);
        const headers = new Headers({'Content-Type': 'application/json'});
        //sends the request only if its subscribed --> Observable
        //url of post-method in routes/app.js
        return this.http.post('http://localhost:4200/api/bill', body, {headers: headers})
            .map((data: Response) => {
                const extracted = data.json();
                return extracted.path;
            })
    }

    uploadBill(params: Array<any>, files: File[]): Observable<any>{
        return Observable.create(observer => {
            let formData: any = new FormData();
            let xhr = new XMLHttpRequest();
            for(let i = 0; i < files.length; i++) {
                formData.append("uploads[]", files[i], files[i].name);
                formData.append("path", params[0].path);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        observer.next(JSON.parse(xhr.response));
                        observer.complete();
                    } else {
                        observer.error(xhr.response);
                    }
                }
            };
            xhr.open("POST", 'http://localhost:4200/api/upload', true);
            xhr.send(formData);
        });
    }
}
