import { TestBed, inject } from '@angular/core/testing';

import { BillService } from './bill.service';

describe('CostumerService', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [BillService]
        });
    });

    it('should ...', inject([BillService], (service: BillService) => {
        expect(service).toBeTruthy();
    }));
});
