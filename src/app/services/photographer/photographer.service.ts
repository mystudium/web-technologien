import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';
import {Photographer} from '../../models/photographer/photographer.model';
import {Customer} from "../../models/customer/customer.model";

@Injectable()
export class PhotographerService {

  constructor(private http: Http) { }

  getPhotographers(): Observable<any> {
    // url of resource -> routes/app.js router.get -> data: messages
    return this.http.get('http://localhost:4200/api/photographers')
        .map((data: Response) => {
          const extracted = data.json(); // data of the Response converted in js-Object
          const photoArray: Photographer[] = [];
          let photo;
          for (const element of extracted.data){
            photo = new Photographer(element._id, element.anrede, element.vorname, element.nachname, element.strasse, element.hausnr,
            element.plz, element.stadt, element.land, element.email, element.telefonnr, element.handynr, element.fax, element.kunden);
            photoArray.push(photo);
          }
          return photoArray; // return transformed data

        }); // Transform data from server
  }

  savePhotographer(photo: Photographer): Observable<any> {
      // Creating body for post-method in routes/app.js
      const body = JSON.stringify(photo);
      const headers = new Headers({'Content-Type': 'application/json'});
      // sends the request only if its subscribed --> Observable
      // url of post-method in routes/app.js
      return this.http.post('http://localhost:4200/api/photographer', body, {headers: headers});
  }

  updateCustomerList(fotograf: Photographer, cos_id: string) {
      const body = {fotograf_id: fotograf._id, customer_id: cos_id};
      const headers = new Headers({'Content-Type': 'application/json'});
      return this.http.post('http://localhost:4200/api/updateCustomerList', body, {headers: headers});
  }
}
