const express = require('express');
const router = express.Router();
var Customer = require('../models/customer');
var User = require('../models/user');
var Photographer = require('../models/photographer');
var bcrypt = require('bcrypt');
var generatePasswd = require('password-generator');
const path = require('path');
const fs = require('fs-extra');
const mkdirp = require('mkdirp');
const sharp = require('sharp');
var conn = mongoose.connection;
var multer = require('multer');
var GridFsStorage = require('multer-gridfs-storage');
var Grid = require('gridfs-stream');
Grid.mongo = mongoose.mongo;
var gfs = Grid(conn.db);
var Bill = require('../models/bill');
const DIR = './src/assets/uploads/test';
/** Setting up storage using multer-gridfs-storage */

var storageDB = GridFsStorage({
    gfs: gfs,
    filename: function(req, file, cb) {
        var datetimestamp = Date.now();
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);

    },
    /** With gridfs we can store aditional meta-data along with the file */
    metadata: function(req, file, cb) {
        cb(null, {
            originalname: file.originalname
        });
    },
    root: 'logoFiles' //root name for collection to store files into
});
var storageDisk = multer.diskStorage({ //multers disk storage settings
    destination: function(req, file, cb) {
        cb(null, DIR);
    },
    filename: function(req, file, cb) {
        var datetimestamp = Date.now();
        console.log(file.fieldname, file.destination);
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
    }
});
//multer settings for single upload
var uploadToDB = multer({
    storage: storageDB
}).single('file');
var uploadToDisk = multer({
    storage: storageDisk
}).single('file');


/* GET api listing. */
router.get('/', (req, res) => {
    res.send('api works');
});

//Fetch data
router.get('/users', function(req, res, next) {
    User.find(function(err, users) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: users
        });
    });
});

//Fetch data
router.get('/customers', function(req, res, next) {
    Customer.find(function(err, customers) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: customers
        });
    });
});

//Fetch data
router.get('/photographers', function(req, res, next) {
    Photographer.find(function(err, photographer) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: photographer
        });
    });
});

router.get('/userByEmail', function(req, res, next) {
    User.findOne({
        email: req.query.email
    }, function(err, usr) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: usr
        });
    });
});

router.get('/customerById', function(req, res, next) {
    console.log('Find by id: ' + req.query._id);
    Customer.find({
        _id: req.query._id
    }, function(err, customer) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: customer
        });
    });
})

router.get('/generatePassword', function(req, res, next) {
    var passwd = generatePasswd(10);
    res.status(200).json({
        data: passwd
    });
});

router.get('/bills', function(req, res, next) {
    Bill.find(function(err, bills) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: bills
        });
    });
});
//Passing actual data, content-field should be available in the request
router.post('/customer', function(req, res, next) {
    var cos = new Customer({
        firmenname: req.body.firmenname,
        anrede: req.body.anrede,
        vorname: req.body.vorname,
        nachname: req.body.nachname,
        //branding: req.body.branding,
        strasse: req.body.strasse,
        hausnr: req.body.hausnr,
        plz: req.body.plz,
        stadt: req.body.stadt,
        land: req.body.land,
        email: req.body.email,
        telefonnr: req.body.telefonnr,
        handynr: req.body.handynr,
        fax: req.body.fax,
        logo: req.body.logo,
        fotografen: req.body.fotografen
    });
    cos.save(function(err, result) {
        if (err) {
            return res.status(500).json({
                message: 'Error while saving Data'
            });
        }
        res.status(201).json({
            message: 'Saved Data successfully',
            resp: result
        });
    });
});

router.post('/updateCustomer', function (req, res, next) {
    Customer.findOneAndUpdate({_id: req.body._id}, {
        firmenname: req.body.firmenname,
        anrede: req.body.anrede,
        vorname: req.body.vorname,
        nachname: req.body.nachname,
        strasse: req.body.strasse,
        hausnr: req.body.hausnr,
        plz: req.body.plz,
        stadt: req.body.stadt,
        land: req.body.land,
        telefonnr: req.body.telefonnr,
        handynr: req.body.handynr,
        fax: req.body.fax,
        fotografen: req.body.fotografen
    }, function (err, result) {
        if (err) {
            return res.status(500).json({
                message: 'Error while updating Data'
            });
        }
        res.status(201).json({
            message: 'Updated Data successfully'
        });
    });
});

router.post('/deleteCustomer', function (req, res, next) {
    Customer.findOneAndRemove({_id: req.body._id}, function (err, result) {
        if (err) {
            return res.status(500).json({
                message: 'Error while deleting Data'
            });
        }
        res.status(201).json({
            message: 'Deleted Data successfully'
        });
    });
});

router.post('/deleteUser', function (req, res, next) {
    User.findOneAndRemove({email: req.body.email}, function (err, result) {
        if (err) {
            return res.status(500).json({
                message: 'Error while deleting Data'
            });
        }
        res.status(201).json({
            message: 'Deleted Data successfully'
        });
    });
});

router.post('/billsByCondition', function(req, res, next) {
    Bill.find({
        'filepath': req.body.condition
    }, function(err, bills) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            data: bills
        });
    });
});

router.post('/updateBills', function(req, res, next) {
    Bill.findOneAndUpdate(req.body.condition, req.body.update, function(err, result) {
        if (err) {
            //ohne return würden beide responses gesendet werden
            return res.status(500).json({
                message: 'Error while fetching data!'
            });
        }
        res.status(200).json({
            message: 'successfully updated!',
            path: result.filepath
        });
    });
});

router.post('/bill', function(req, res, next) {
    console.log(req.body.filepath);
    //body siehe bill.service
    var bill = new Bill({
        filepath: req.body.filepath,
        date: req.body.date,
        paid: req.body.paid,
        sum: req.body.sum
    });

    bill.save(function(err, result) {
        if (err) {
            console.log(err);
            return res.status(500).json({
                message: 'Error while saving data!'
            });
        }
        res.status(201).json({
            message: 'Saved data successfully!'
        });
    });
});

router.post('/user', function(req, res, next) {
    var user = new User({
        connection: req.body.connection,
        debug: req.body.debug,
        email: req.body.email,
        password: req.body.password,
        tenant: req.body.tenant,
        role: req.body.role
    });
    User.findOne({
        email: user.email
    }, function(err, sameEmail) {
        if (err) {
            return res.status(500).json({
                message: 'Error while saving data!'
            });
        }
        if (sameEmail) {
            return res.status(500).json({
                message: 'User already exists'
            });
        }

        bcrypt.hash(user.password, 10, function(err, hash) {
            if (err) {
                return res.status(500).json({
                    message: 'Error while saving data!'
                });
            }
            user.password = hash;
            user.save(function(err, result) {
                if (err) {
                    return res.status(500).json({
                        message: 'Error while saving data!'
                    });
                }
                res.status(201).json({
                    message: 'Saved data successfully!'
                });
            });
        });
    });
});

router.post('/photographer', function(req, res, next) {
    var p = new Photographer({
        anrede: req.body.anrede,
        vorname: req.body.vorname,
        nachname: req.body.nachname,
        strasse: req.body.strasse,
        hausnr: req.body.hausnr,
        plz: req.body.plz,
        stadt: req.body.stadt,
        land: req.body.land,
        email: req.body.email,
        telefonnr: req.body.telefonnr,
        handynr: req.body.handynr,
        fax: req.body.fax
    });
    p.save(function(err, result) {
        if (err) {
            return res.status(500).json({
                message: 'Error while saving data!'
            });
        }
        res.status(201).json({
            message: 'Saved data successfully!'
        });
    });
});

router.post('/updateCustomerList', function (req, res, next) {
    Photographer.findOne({_id: req.body.fotograf_id}, function (err, photographer) {
        if (err) {
            return res.status(500).json({
                message: 'Photographer not found!'
            });
        }
        if(!photographer.kunden){
            photographer.kunden = [];
        }
        photographer.kunden.push(req.body.customer_id);
        Photographer.findOneAndUpdate({_id: req.body.fotograf_id}, {kunden: photographer.kunden}, function (err, success) {
            if (err) {
                return res.status(500).json({
                    message: 'Update failed!'
                });
            }
            res.status(200).json({
                message: 'Saved data successfully!'
            });
        })

    })
});

router.post('/upload', multer({
    dest: './src/assets/uploads'
}).array("uploads[]", 12), function(req, res) {
    var dest_path = path.join(req.files[0].destination, req.body.path.split('/')[0]);
    mkdirp(dest_path, function() {
        fs.readFile('./' + req.files[0].path, function(err, data) {
            fs.writeFile(dest_path + '/' + req.files[0].originalname, data, function(err) {
                fs.unlink('./' + req.files[0].path, function() {
                    if (err) throw err;
                });
            });
        });
    });
    res.send(req.files)
});

router.post('/uploadGallery', multer({
    dest: './src/assets/uploads'
}).array("uploads[]", 12), function(req, res) {
    var dest_path = path.join(req.files[0].destination, req.body.path.split('/')[0]);
    mkdirp(dest_path.toString, function() {
        fs.readFile('./' + req.files[0].path, function(err, data) {
            fs.writeFile(dest_path + '/' + req.files[0].originalname, data, function(err) {
                fs.unlink('./' + req.files[0].path, function() {
                    if (err) throw err;
                });
            });
        });

    });
    res.send(req.files)
});



router.post('/testapi', function(req, res) {
    uploadToDisk(req, res, function(err) {
        if (err) {
            res.status(501).json({
                message: 'This is an internal Server Error, for Dagny'
            });
            return;
        }
        console.log(req.file.filename);
        var subdir = '/newtest/';
        var filename = req.file.filename;
        var prefix = 'web_';
        fs.moveSync(DIR + '/' + filename, DIR + subdir + filename, function(err) {
            if (err) {
                return console.error(err);
            }
        });
        var inputfile = DIR + subdir + filename;
        var outputfile = DIR + subdir + prefix + filename;
        sharp(inputfile)
            .resize(200, 200)
            .toFile(outputfile, function(err, res) {
                if (!err) {
                    fs.unlinkSync(inputfile);
                }
            });

        res.status(200).json({
            message: 'File Uploaded'
        });
    });
});
/** API path that will upload the files */
router.post('/logoUpload', function(req, res) {
    uploadToDB(req, res, function(err) {
        if (err) {
            res.status(500).json({
                message: "This is a mess, your fault"
            });
            return;
        }
        res.status(200).json({
            id: req.file.id
        });
    });
});

router.get('/file/:filename', function(req, res) {
    gfs.collection('logoFiles'); //set collection name to lookup into

    /** First check if file exists */
    gfs.files.find({ filename: req.params.filename }).toArray(function(err, files) {
        if (!files || files.length === 0) {
            return res.status(404).json({
                responseCode: 1,
                responseMessage: "error"
            });
        }
        /** create read stream */
        var readstream = gfs.createReadStream({
            filename: files[0].filename,
            root: "logoFiles"
        });
        /** set the proper content type */
        res.set('Content-Type', files[0].contentType)
            /** return response */
        return readstream.pipe(res);
    });
});
module.exports = router;