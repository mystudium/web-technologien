"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Created by hkosbab on 16.06.17.
 */
var sharp = require("sharp");
var convert = (function () {
    function convert() {
    }
    /*settings = {'size':null, 'sigma':null};


    constructor (_size: number, _sigma: number) {
        this.settings.sigma=_sigma;
        this.settings.size=_size;
    }*/
    convert.prototype.imageConvert = function (img, output) {
        sharp(img).rotate().toFile(output);
    };
    return convert;
}());
exports.convert = convert;
