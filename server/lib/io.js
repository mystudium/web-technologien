"use strict";
/**
 * Created by hkosbab on 16.06.17.
 */
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var io = (function () {
    function io() {
        this.images = [];
        this.convert = require("./convert");
        this.prefix = 'web_';
        this.filter = '.jpg';
    }
    io.prototype.getImageFolder = function (working_path) {
        var self = this;
        var foldernames = fs.readdirSync(working_path);
        foldernames.forEach(function (name) {
            var tmpPath = path.join(working_path, name).toString();
            if (name === '.' || name === '..' || name.startsWith(".")) {
            }
            if (!(fs.lstatSync(tmpPath).isDirectory())) {
                if (name.indexOf(self.filter) >= 0) {
                    var outputFile = path.join(working_path, self.prefix + name);
                    console.log(tmpPath, outputFile);
                    self.convert.imageConvert(tmpPath, outputFile);
                    self.images.push(outputFile);
                }
            }
            if (fs.lstatSync(tmpPath).isDirectory()) {
                self.getImageFolder(tmpPath);
            }
        });
    };
    return io;
}());
exports.io = io;
