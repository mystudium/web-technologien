/**
 * Created by hkosbab on 16.06.17.
 */


import * as fs from  'fs';
import * as path  from 'path';

export class io {

	public images:string[]=[];
	public convert = require("./convert");
	public prefix = 'web_';
    public filter = '.jpg';

	getImageFolder(working_path:string):void {
		let self = this;
		let foldernames = fs.readdirSync(working_path);
		foldernames.forEach(function (name) {
			let tmpPath = path.join(working_path, name).toString();
			if (name === '.' || name === '..' || name.startsWith(".")) {
			}
			if (!(fs.lstatSync(tmpPath).isDirectory())) {
				if (name.indexOf(self.filter) >= 0) {
					let outputFile:string = path.join(working_path,self.prefix+name);
					console.log(tmpPath,outputFile);
                    self.convert.imageConvert(tmpPath,outputFile);
                    self.images.push(outputFile);

				}
			}
			if (fs.lstatSync(tmpPath).isDirectory()) {
				self.getImageFolder(tmpPath);
			}


		});
	}
}

