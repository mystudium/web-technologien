var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    connection: {type: String},
    debug: {type: Boolean},
    email: {type: String},
    password: {type: String},
    tenant: {type: String},
    role: {type: String}
});

module.exports = mongoose.model('User', schema);