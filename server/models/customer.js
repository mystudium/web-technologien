var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    firmenname: {type: String, required: true},
    anrede: {type: String, required: true},
    vorname: {type: String, required: true},
    nachname: {type: String, required: true},
    //branding: {type: string, required: false},
    strasse: {type: String, required: true},
    hausnr: {type: String, required: true},
    plz: {type: Number, required: true},
    stadt: {type: String, required: true},
    land: {type: String, required: true},
    email: {type: String, required: true},
    telefonnr: {type: String, required: false},
    handynr: {type: String, required: false},
    fax: {type: String, required: false},
    logo: {type: Schema.Types.ObjectId, ref:'logoFiles', required: false},
    fotografen: [{type: Schema.Types.ObjectId, ref: 'Photographer'}]
});

module.exports = mongoose.model('Customer', schema);

/*
* fotografen: {type: [Photographer], required: false},
 rechnungen: {type: [Bill], required: false},
 galerien: {type: [Galery], required: false}
* */