var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    filepath: {type: String, required: true},
    date: {type: Date, required: true},
    paid: {type: Boolean, required: true},
    sum: {type: Number, required: true}
});

module.exports = mongoose.model('Bill', schema);
