var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    anrede: {type: String, required: true},
    vorname: {type: String, required: true},
    nachname: {type: String, required: true},
    strasse: {type: String, required: true},
    hausnr: {type: String, required: true},
    plz: {type: Number, required: true},
    stadt: {type: String, required: true},
    land: {type: String, required: true},
    email: {type: String, required: true},
    telefonnr: {type: String, required: false},
    handynr: {type: String, required: false},
    fax: {type: String, required: false},
    kunden: [{type: Schema.Types.ObjectId, ref: 'Customer'}]
});

module.exports = mongoose.model('Photographer', schema);

/*
 * kunden: {type: [Customer], required: false},
 * */